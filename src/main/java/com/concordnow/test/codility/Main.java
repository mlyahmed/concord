package com.concordnow.test.codility;

import com.sun.tools.javac.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {


    public static void main(String [] args) {
        final String employees = "John Doe, Peter Parker, Mary Jane Watson-Parker, James Doe, John Elvis Doe, Jane Doe, Penny Parker";
        final String emails = "John Doe <j_doe@example.com>, Peter Parker <p_parker@example.com>, Mary Jane Watson-Parker <m_j_watsonpa@example.com>, James Doe <j_doe2@example.com>, John Elvis Doe <j_e_doe@example.com>, Jane Doe <j_doe3@example.com>, Penny Parker <p_parker2@example.com>";
        final String result = solution(employees, "example");

        System.out.println(emails);
        System.out.println(result);
        Assert.check(emails.equals(result));
    }


    public static String solution(String S, String T) {

        final List<String> emails = new ArrayList<>();
        final String output = Arrays.stream(S.split(", "))
                .map(e -> buildEmail(e, T))
                .map(e -> {
                    final String sanitized = sanitizeEmail(e, emails);
                    emails.add(sanitized);
                    return sanitized;
                })
                .collect(Collectors.joining(", "));

        return output;
    }


    private static String buildEmail(String name, String company) {
        final String email = getFirstNamePart(name) + getMiddleNamePart(name) + getLastNamePart(name) + "@" + company + ".com";
        return combineEmailAndName(email.toLowerCase(), name); // We could do it better !!!
    }


    private static String getFirstNamePart(String name) {
        return toParts(name)[0].substring(0, 1) + "_";
    }


    private static String getMiddleNamePart(String name) {
        return toParts(name).length == 3 ? toParts(name)[1].substring(0, 1) + "_" : "";
    }

    private static String getLastNamePart(String name) {
        final String lastname = toParts(name)[toParts(name).length - 1];
        return lastname.length() > 7 ? lastname.substring(0, 8) : lastname;
    }


    private static String[] toParts(String name) {
        final String remp = name.replaceAll("-", "");
        return remp.split(" ");
    }


    private static String sanitizeEmail(String email, List<String> repo) {
        final String[] parts = email.split("<")[1].split("@");
        final long count = repo.stream().filter(e -> e.contains(parts[0])).count();
        return count > 0 ? email.replaceAll ("@" , (count + 1)+"@") : email;
    }


    private static String combineEmailAndName(String email, String name) {
        return name + " <" + email + ">";
    }



}
